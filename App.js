/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
 
import About from './src/Screen/About';
import Home from './src/Screen/Home';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Dashboard from './src/Screen/Dashboard';

const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const TabsNav = () => {
  return (
    <Drawer.Navigator initialRouteName="dashboard">
      <Drawer.Screen name="dashboard" component={Dashboard} />
      <Drawer.Screen name="home" component={Home} />
      <Drawer.Screen name="about" component={About} />
    </Drawer.Navigator>
  );
};
const App = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="dashboard" component={TabsNav} />
        <Tab.Screen name="home" component={Home} />
        <Tab.Screen name="about" component={About} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;
