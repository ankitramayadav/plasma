import React from 'react';
import {Button, TouchableOpacity, View, Text} from 'react-native';

export default function About({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <TouchableOpacity>
        <Text>This is About</Text>
      </TouchableOpacity>
    </View>
  );
}
