import React from 'react';
import {Image, ImageBackground, Text, View} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons';
import IconION from 'react-native-vector-icons/Ionicons';

export default function Dashboard() {
  return (
    <ImageBackground
      source={require('../images/back.png')}
      style={{width: '100%', height: '100%'}}>
      <View
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          flexDirection: 'row',
          marginTop: 40,
          alignItems: 'center',
          paddingHorizontal: 40,
        }}>
        <IconAnt style={{color: '#a2a2db'}} name="menuunfold" size={20} />
        <IconAnt style={{color: '#a2a2db'}} name="profile" size={20} />
      </View>

      <View style={{marginTop: 40, paddingHorizontal: 40}}>
        <Text style={{fontSize: 40, color: '#522289'}}>Hello</Text>
        <Text
          style={{
            fontSize: 15,
            paddingVertical: 10,
            lineHeight: 22,
            color: '#a2a2db',
          }}>
          welcome to Exits!
        </Text>

        <View
          style={{
            flexDirection: 'row',
            backgroundColor: '#fff',
            borderRadius: 40,
            alignItems: 'center',
            paddingHorizontal: 20,
            marginTop: 40,
          }}>
          <Image
            source={require('../images/search.png')}
            style={{height: 14, width: 14}}
          />
          <TextInput
            placeholder="Exits consultancy"
            style={{
              paddingHorizontal: 20,
              fontSize: 15,
              color: '#ccccef',
            }}
          />
        </View>

        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{marginRight: -40, marginTop: 30}}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 66,
              width: 66,
              borderRadius: 50,
              backgroundColor: '#5facdb',
            }}>
            <Image
              source={require('../images/p.png')}
              style={{height: 24, width: 24}}
            />
          </View>

          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 66,
              width: 66,
              borderRadius: 50,
              backgroundColor: '#ff5c83',
              marginHorizontal: 22,
            }}>
            <IconMCI name="office-building" color="white" size={32} />
          </View>

          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 66,
              width: 66,
              borderRadius: 50,
              backgroundColor: '#ffa06c',
            }}>
            <IconMCI name="bus" color="white" size={32} />
          </View>

          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 66,
              width: 66,
              borderRadius: 50,
              backgroundColor: '#bb32fe',
              marginLeft: 22,
            }}>
            <IconMCI name="dots-horizontal" color="white" size={32} />
          </View>
        </ScrollView>

        <Text
          style={{
            color: '#FFF',
            marginTop: 50,
            fontSize: 17,
          }}>
          Recommended
        </Text>

        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{marginHorizontal: -40, marginTop: 30}}>
          <View
            style={{
              backgroundColor: '#FEFEFE',
              height: 200,
              width: 190,
              borderRadius: 15,
              padding: 5,
              marginHorizontal: 20,
            }}>
            <Image
              source={require('../images/1.jpg')}
              style={{width: 180, height: 130, borderRadius: 10}}
            />
            <View
              style={{
                paddingHorizontal: 20,
                paddingVertical: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 11, color: '#a2a2db'}}>
                welcome to New world of IOT.welcome to New world of IOT.
              </Text>
              <IconMCI
                name="google-maps"
                color="red"
                size={25}
                style={{paddingVertical: 5}}
              />
            </View>
          </View>
          <View
            style={{
              backgroundColor: '#FEFEFE',
              height: 200,
              width: 190,
              borderRadius: 15,
              padding: 5,
              marginHorizontal: 20,
            }}>
            <Image
              source={require('../images/2.jpg')}
              style={{width: 180, height: 130, borderRadius: 10}}
            />
            <View
              style={{
                paddingHorizontal: 20,
                paddingVertical: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 11, color: '#a2a2db'}}>
                welcome to New world of IOT.welcome to New world of IOT.
              </Text>
              <IconMCI
                name="google-maps"
                color="red"
                size={25}
                style={{paddingVertical: 5}}
              />
            </View>
          </View>
          <View
            style={{
              backgroundColor: '#FEFEFE',
              height: 200,
              width: 190,
              borderRadius: 15,
              padding: 5,
              marginHorizontal: 20,
            }}>
            <Image
              source={require('../images/3.jpg')}
              style={{width: 180, height: 130, borderRadius: 10}}
            />
            <View
              style={{
                paddingHorizontal: 20,
                paddingVertical: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 11, color: '#a2a2db'}}>
                welcome to New world of IOT.welcome to New world of IOT.
              </Text>
              <IconMCI
                name="google-maps"
                color="red"
                size={25}
                style={{paddingVertical: 5}}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </ImageBackground>
  );
}
