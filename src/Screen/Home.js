import React from 'react';
import { View, Text, StyleSheet} from 'react-native';
import Boxes from './Boxes';
import Header from './Layout/Header';

export default function Home({navigation}) {
  return (
    <View style={styles.container}> 
      <Header />
      <Boxes />
    </View>
  );
}


const styles = StyleSheet.create({
  container : { 
    flex :1, 
  }
})