import {createStackNavigator} from '@react-navigation/stack';
import About from '../Screen/About';
import Detail from '../Screen/Detail';
import Home from '../Screen/Home';
import ListCard from '../Screen/ListCard';
import ModalCard from '../Screen/ModalCard';

const Stack = createStackNavigator();

function Navigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="About" component={About} />
      <Stack.Screen name="Detail" component={Detail} />
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="ListCard" component={ListCard} />
      <Stack.Screen name="ModalCard" component={ModalCard} />
    </Stack.Navigator>
  );
}
